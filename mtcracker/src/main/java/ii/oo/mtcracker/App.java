package ii.oo.mtcracker;

import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;
import org.mindrot.jbcrypt.BCrypt;

import ii.oo.mtcracker.thread.MultiThreadCracker;
import ii.oo.mtcracker.thread.cracker.CallableBCryptTest;
import ii.oo.mtcracker.thread.cracker.CallableCracker;
import ii.oo.mtcracker.thread.cracker.CallableMD5Test;

/**
 * Example usage
 * 
 * @author batou
 *
 */
public class App {

	public static void main(String[] args) throws Exception {
		String plainTextOriginal = "calum";
		String hashed = getBCryptHashedPassword(plainTextOriginal);
		MultiThreadCracker<? extends CallableCracker> cracker = new MultiThreadCracker<CallableBCryptTest>(
				"abcdefghijklmnopqrstuvwxyz", 5, 5, hashed, CallableBCryptTest.class);
		long start = System.currentTimeMillis();
		String plainTextCracked = cracker.crackPassword();
		System.out.println(plainTextCracked + " cracked in " + (System.currentTimeMillis() - start) + "ms for BCrypt");

		hashed = getMD5HashedPassword(plainTextOriginal);
		cracker = new MultiThreadCracker<CallableMD5Test>("abcdefghijklmnopqrstuvwxyz", 5, 5, hashed,
				CallableMD5Test.class);
		start = System.currentTimeMillis();
		plainTextCracked = cracker.crackPassword();
		System.out.println(plainTextCracked + " cracked in " + (System.currentTimeMillis() - start) + "ms for MD5");

	}

	private static String getBCryptHashedPassword(String plainText) {
		String hashedPassword = BCrypt.hashpw(plainText, BCrypt.gensalt());
		return hashedPassword;
	}

	private static String getMD5HashedPassword(String plainText) throws Exception {
		byte[] md5Bytes = MessageDigest.getInstance("MD5").digest(plainText.getBytes("UTF-8"));
		return Base64.encodeBase64String(md5Bytes);
	}

}
