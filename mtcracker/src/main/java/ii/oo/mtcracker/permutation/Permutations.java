package ii.oo.mtcracker.permutation;

import java.util.HashSet;
import java.util.Set;

/**
 * Permutations calculates all permutations (including repetitions) of a charset
 * up to a given length. Can be used like an iterator and if all the
 * permutations weren't calculated you can enter the state that was left and
 * start execution from there.
 * 
 * @author batou
 *
 */
public class Permutations {

	private char[] charset;
	private int maxLength;
	private int currentLength;
	private int[] currentIndexes;

	/**
	 * Start generating permutations from the charset provided up to a maximum
	 * length.
	 * 
	 * @param charset
	 * @param maxLength
	 */
	public Permutations(String charset, int maxLength) {
		this.maxLength = maxLength;
		// Add the charset to a hashset to remove any duplicates
		Set<Character> characters = new HashSet<>();
		for (char c : charset.toCharArray()) {
			characters.add(c);
		}
		this.charset = new char[characters.size()];
		int i = 0;
		for (char c : characters) {
			this.charset[i] = c;
			i++;
		}
		this.currentLength = 1;
		generateIndexes();
	}

	/**
	 * Start the permutations where you left off.
	 * 
	 * @param charset
	 * @param maxLength
	 * @param currentIndexes
	 * @param currentLength
	 */
	public Permutations(char[] charset, int maxLength, int[] currentIndexes, int currentLength) {
		this.charset = charset;
		this.maxLength = maxLength;
		this.currentIndexes = currentIndexes;
		this.currentLength = currentLength;
	}

	/**
	 * get the charset set
	 * 
	 * @return
	 */
	public char[] getCharset() {
		return this.charset;
	}

	/**
	 * Are we calculating anymore ?
	 * 
	 * @return true if we haven't reached past the maximum length set
	 */
	public synchronized boolean hasNext() {
		// we have more to calculate unless we're about to create a string
		// larger than asked for
		return (currentLength <= maxLength);
	}

	/**
	 * Get the next permutation.
	 * 
	 * @return the next permutation of this charset
	 */
	public synchronized String next() {
		String output = "";
		// Generate a string based on our current indexes in the current
		// combination
		for (int i = 0; i < currentLength; i++) {
			output += charset[currentIndexes[i]];
		}
		// if we have reached a maxed out set of indexes then increment length
		if (maxedOutIndexes()) {
			currentLength++;
			generateIndexes();
		} else {
			// else work out the next set of indexes
			incrementIndexesByOne();
		}
		return output;
	}

	/**
	 * Check if we are at [charset.length][charset.length][charset.length]...
	 * <br>
	 * Meaning we would have maxed out this length.
	 * 
	 * @return true if we have reached the last permutation of this length
	 */
	private boolean maxedOutIndexes() {
		for (int i : currentIndexes) {
			if (i != charset.length - 1)
				return false;
		}
		return true;
	}

	/**
	 * Increment character indexes by one, starts rightmost character and on
	 * reaching the maximum character index increments the next level by one.
	 */
	private void incrementIndexesByOne() {
		for (int i = currentIndexes.length - 1; i >= 0; i--) {
			currentIndexes[i]++;
			if (currentIndexes[i] == charset.length) {
				currentIndexes[i] = 0;
			} else {
				return;
			}
		}
	}

	/**
	 * Build character index array for this length.
	 */
	private void generateIndexes() {
		// by default arrays of ints are initialized with 0's
		this.currentIndexes = new int[currentLength];
	}

}
