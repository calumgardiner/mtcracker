package ii.oo.mtcracker.thread;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ii.oo.mtcracker.permutation.Permutations;
import ii.oo.mtcracker.thread.cracker.CallableCracker;

/**
 * Multi threaded cracker uses the callable cracker interface to create futures,
 * fires off a thread which checks the futures as they complete.
 * 
 * @author batou
 *
 * @param <C>
 */
public class MultiThreadCracker<C extends CallableCracker> {

	private Permutations combinator;
	private ExecutorService threadPool;
	private String hash;

	private Class<C> cracker;

	private volatile Boolean running;

	private List<Future<String>> results;

	private Runnable resultCheckThread;

	private String crackedPassword;

	public MultiThreadCracker(String charset, int maxLength, int threads, String hash, Class<C> cracker) {
		this.combinator = new Permutations(charset, maxLength);
		this.threadPool = Executors.newFixedThreadPool(threads);
		this.hash = hash;
		this.cracker = cracker;
		this.results = Collections.synchronizedList(new ArrayList<>());
		this.running = new Boolean(true);
		this.resultCheckThread = new RunnableResultChecker();
		Thread t = new Thread(resultCheckThread);
		t.start();
	}

	public String crackPassword() throws Exception {
		try {
			while (running) {
				if (results.size() < 10000) {
					String plainTest = combinator.next();
					C cracker = this.cracker.newInstance();
					cracker.setHashedPassword(hash);
					cracker.setPlainTextTest(plainTest);
					Future<String> futureResult = this.threadPool.submit(cracker);
					this.results.add(futureResult);
				}
			}
			return crackedPassword;
		} finally {
			this.threadPool.shutdownNow();
		}
	}

	private class RunnableResultChecker implements Runnable {

		@Override
		public void run() {
			while (running) {
				synchronized (results) {
					if (!results.isEmpty()) {
						Future<String> result = results.get(0);
						try {
							if (result.get() != null) {
								MultiThreadCracker.this.crackedPassword = result.get();
								MultiThreadCracker.this.running = false;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						results.remove(0);
					}
				}
			}
		}

	}
}
