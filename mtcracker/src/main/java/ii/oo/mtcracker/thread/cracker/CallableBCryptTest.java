package ii.oo.mtcracker.thread.cracker;

import org.mindrot.jbcrypt.BCrypt;

/**
 * Bcrypt implementation of a callable cracker thread
 * 
 * @author batou
 *
 */
public class CallableBCryptTest implements CallableCracker {

	private String testString;
	private String hash;

	public CallableBCryptTest() {
	}

	public CallableBCryptTest(String testString, String hash) {
		setHashedPassword(hash);
		setPlainTextTest(testString);

	}

	public String call() throws Exception {
		if (BCrypt.checkpw(testString, hash))
			return testString;
		return null;
	}

	@Override
	public void setHashedPassword(String password) {
		this.hash = password;
	}

	@Override
	public void setPlainTextTest(String plainTest) {
		this.testString = plainTest;
	}

}
