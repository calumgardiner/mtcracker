package ii.oo.mtcracker.thread.cracker;

import java.util.concurrent.Callable;

/**
 * Callable cracker interface, set a plaintext to test against the specified
 * hash.
 * 
 * @author batou
 *
 */
public interface CallableCracker extends Callable<String> {

	public void setHashedPassword(String password);

	public void setPlainTextTest(String plainTest);

}
