package ii.oo.mtcracker.thread.cracker;

import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;

public class CallableMD5Test implements CallableCracker {
	
	private String password;
	
	private String plainTest; 

	@Override
	public String call() throws Exception {
		byte[] md5Bytes = MessageDigest.getInstance("MD5").digest(plainTest.getBytes("UTF-8"));
		String encoded = Base64.encodeBase64String(md5Bytes);
		if (encoded.equals(password)) 
			return plainTest;
		return null;
	}

	@Override
	public void setHashedPassword(String password) {
		this.password = password;
	}

	@Override
	public void setPlainTextTest(String plainTest) {
		this.plainTest = plainTest;
	}

}
